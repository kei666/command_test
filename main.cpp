#include <boost/program_options.hpp>
#include <iostream>



int main(int argc, char **argv) {
    namespace po = boost::program_options;

    po::options_description opt("");

    opt.add_options()
            ("help,h", "ヘルプを表示")
            ("string,s", po::value<std::string>(), "文字列");

    po::variables_map vm;

    try {
        po::store(po::parse_command_line(argc, argv, opt), vm);
    } catch (const po::error_with_option_name &e) {
        std::cout << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    po::notify(vm);

    if (vm.count("help")) {
        std::cout << opt << std::endl;
        return EXIT_SUCCESS;
    }

    if (vm.count("string")) {
        std::string str = vm["string"].as<std::string>();
        std::cout << str << std::endl;
        return EXIT_SUCCESS;
    }

    return EXIT_SUCCESS;
}